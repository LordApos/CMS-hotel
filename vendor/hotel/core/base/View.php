<?php
/**
 * Created by PhpStorm.
 * User: Apos
 * Date: 12.04.2018
 * Time: 18:52
 */

namespace hotel\base;


class View
{
    public $route;
    public $controller;
    public $model;
    public $layout;
    public $view;
    public $prefix;
    public $data = [];
    public $meta = [];

    public function __construct($route, $layout = '', $view = '', $meta) {
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->model = $route['controller'];
        $this->view = $view;
        $this->prefix = $route['prefix'];
        $this->meta = $meta;
        if($layout === false){
            $this->layout = false;
        }else{
            $this->layout = $layout ?: LAYOUT;
        }
    }
    public function render($data){
        if(is_array($data)) extract($data);
        $file_view2 = APP . "/views/{$this->prefix}{$this->controller}/{$this->view}.php";
        $file_view = preg_replace('/\\\\+/','/',$file_view2);
        if(is_file($file_view)){
            ob_start();
            require_once $file_view;
            $content = ob_get_clean();
        }else{
            throw new\Exception("He найден вид {$file_view}", 500);
        }
        if(false !== $this->layout) {
            $file_layout = APP . "/views/layouts/{$this->layout}.php";
            if(is_file($file_layout)){
                require_once $file_layout;
            }else{
                throw new\Exception( "Не найден шаблон {$this->layout}", 500);
            }
        }
    }
    public function getMeta(){
        $output = '<title>' . $this->meta['title'] . '</title>' . PHP_EOL;
        $output .= '<meta name="description" content="' . $this->meta['desc'] . '">' . PHP_EOL;
        $output .= '<meta name="keywords" content="' . $this->meta['keywords'] . '">' . PHP_EOL;
        return $output;
    }
}