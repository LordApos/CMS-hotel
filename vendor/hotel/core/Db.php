<?php
/**
 * Created by PhpStorm.
 * User: Apos
 * Date: 13.04.2018
 * Time: 18:47
 */

namespace hotel;
use \RedBeanPHP\R;

class Db extends \RedBeanPHP\SimpleModel
{
    use TSingletone;

    protected function __construct() {
        $db = require ROOT . '/config/config_db.php';
        R::setup($db['dsn'], $db['user'], $db['pass']);
        if ( !R::testConnection()){
            throw new \Exception("Нет соединения с БД", 500);
        }
        R::freeze(true);
        if (DEBUG){
            R::debug(true, 1);
        }


        /*
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        ];
        $this->pdo = new \PDO($db['dsn'], $db['user'], $db['pass'], $options);*/
    }



    /*
    protected $pdo;
    protected static $instance;
    public static $countSql = 0;
    public static $queries = [];




    public static function instance() {
        if(self::$instance === null){
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function execute($sql, $params = []){
        self::$countSql++;
        self::$queries[] = $sql;
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute($params);
    }

    public function query($sql, $params = []) {
        self::$countSql++;
        self::$queries[] = $sql;
        $stmt = $this->pdo->prepare($sql);
        $res = $stmt->execute($params);
        if($res !== false){
            return $stmt->fetchAll();
        }
        return [];
    }
    public function querytest($sql, $params = []) {
        self::$countSql++;
        self::$queries[] = $sql;
        $stmt = $this->pdo->prepare($sql);
        $res = $stmt->execute($params);
    }
     */
}