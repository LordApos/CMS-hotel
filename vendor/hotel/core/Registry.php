<?php
/**
 * Created by PhpStorm.
 * User: Apos
 * Date: 11.04.2018
 * Time: 15:58
 */

namespace hotel;


class Registry
{
    use TSingletone;

    protected static $properties = [];

    public function setProperty($name, $value) {
            self::$properties[$name] = $value;
    }
    public function getProperty($name) {
        if(isset(self::$properties[$name])){
            return self::$properties[$name];
        }
        return null;
    }
    public function getProperties(){
        return self::$properties;
    }

}
//    protected static $instance;
/*
    protected function __construct() {
        require_once ROOT . '/config/config.php';
        foreach($config['components'] as $name => $component){
            self::$objects[$name] = new $component;
        }
    }

    /*public static function instance() {
        if(self::$instance === null){
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __getProperty($name) {
        if(is_object(self::$properties[$name])){
            return self::$properties[$name];
        }
    }

    public function __setProperty($name, $value) {
        if(!isset(self::$objects[$name])){
            self::$properties[$name] = new $value;
        }
    }

    public function getList(){
        echo '<pre>';
        var_dump(self::$pr);
        echo '</pre>';
    }
}
*/