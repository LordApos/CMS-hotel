<?php
/**
 * Created by PhpStorm.
 * User: Apos
 * Date: 03.05.2018
 * Time: 9:26
 */

namespace app\controllers;


use app\models\Cart;
use app\models\Order;
use app\models\User;
use RedBeanPHP\R;

class CartController extends AppController
{
    public function addAction(){
        $id = !empty($_GET['id']) ? (int)$_GET['id'] : null;
        $qty = !empty($_GET['qty']) ? (int)$_GET['qty'] : null;
        if ($id){
            $rooms = R::findOne('rooms', 'id = ?', [$id]);
        }
        $cart = new Cart();
        $cart->addToCart($rooms, $qty);
        if ($this->isAjax()){
            $this->loadView('cart_modal');
        }
        redirect();
    }

    public function showAction(){
        $this->loadView('cart_modal');
    }

    public function deleteAction(){
        $id = !empty($_GET['id']) ? $_GET['id'] : null;
        if (isset($_SESSION['cart'][$id])){
            $cart = new Cart();
            $cart->deleteItem($id);
        }
        if ($this->isAjax()){
            $this->loadView('cart_modal');
        }
        redirect();
    }

    public function clearAction(){
        unset($_SESSION['cart']);
        unset($_SESSION['cart.qty']);
        unset($_SESSION['cart.sum']);
        unset($_SESSION['cart.currency']);
        if ($this->isAjax()){
            $this->loadView('cart_modal');
        }
        redirect();
    }
    public function viewAction(){
        $dateminst = date('Y-m-d');
        $d1 = date('Y');
        $d2 = date('m');
        $d3 = date('d');
        $dateminf = $d1 . '-' . $d2 . '-' . ((int)$d3 + 1);
        $this->setMeta('Оформление заказа');
        $this->set(compact('dateminst', 'dateminf'));
    }
    public function checkoutAction(){
        if (!empty($_POST)){
            //reg acc
            if (!User::checkAuth()){
                $user = new User();
                $data = $_POST;
                $user->load($data);
                if (!$user->validate($data) || !$user->checkUnique()){
                    $user->getErrors();
                    $_SESSION['form_data'] = $data;
                    redirect();
                }else{
                    $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
                    if (!$user_id = $user->save('user')){
                        $_SESSION['error'] = 'Ошибка!';
                        redirect();
                    }
                }
            }
            //booking
            $data['user_id'] = isset($user_id) ? $user_id : $_SESSION['user']['id'];
            $data['note'] = !empty($_POST['note']) ? $_POST['note'] : '';
            $data['datestart'] = !empty($_POST['datestart']) ? $_POST['datestart'] : '';
            $data['datefinish'] = !empty($_POST['datefinish']) ? $_POST['datefinish'] : '';
            $user_email = isset($_SESSION['user']['email']) ? $_SESSION['user']['email'] : $_POST['email'];
            $order_id = Order::saveOrder($data);
            if (!empty($order_id)){
                Order::mailOrder($order_id, $user_email);
            }
        }
        redirect();


    }


}