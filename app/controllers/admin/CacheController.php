<?php


namespace app\controllers\admin;


use hotel\Cache;

class CacheController extends AppController{

    public function indexAction(){

        $this->setMeta('Очистка кэша');
    }

    public function deleteAction(){
        $key = isset($_GET['key']) ? $_GET['key'] : null;
        $cache = Cache::instance();
        switch ($key){
            case 'category':
                $cache->delete('cats');
                $cache->delete('admin_select');
                $cache->delete('admin_cat');
                $cache->delete('hotel');
                break;
        }
        $_SESSION['success'] = 'Выбраный кэш удален';
        redirect();
    }
}