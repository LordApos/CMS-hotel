<?php

namespace app\controllers\admin;


use RedBeanPHP\R;

class MainController extends AppController {

    public function indexAction(){
        $countNewOrders = R::count('order', "status = '0'");
        $countUsers = R::count('user');
        $countRooms = R::count('rooms');
        $countCategories = R::count('category');
        $this->setMeta('Админка', 'DescHome', 'bestHotel');
        $this->set(compact( 'countNewOrders', 'countUsers', 'countRooms', 'countCategories'));
    }
}