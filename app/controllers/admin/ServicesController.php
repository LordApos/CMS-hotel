<?php

namespace app\controllers\admin;


use app\models\admin\Services;
use hotel\App;
use RedBeanPHP\R;

class ServicesController extends AppController{

    public function indexAction(){
        $services = R::findAll('services');
        $this->set(compact('services'));
        $this->setMeta('Список услуг');
    }
    public function addImageAction(){
        if(isset($_GET['upload'])){
            $wmax = App::$app->getProperty('service_width');
            $hmax = App::$app->getProperty('service_height');
            $name = $_POST['name'];
            //debug($name . $wmax . $hmax, 1);
            $services = new Services();
            $services->uploadImg($name, $wmax, $hmax);
        }
    }
    public function addAction(){
        if(!empty($_POST)){
            $services = new Services();
            $data = $_POST;
            $services->load($data);
            $services->attributes['status'] = $services->attributes['status'] ? '1' : '0';
            $services->getImg();

            if(!$services->validate($data)){
                $services->getErrors();
                $_SESSION['form_data'] = $data;
                redirect();
            }
            if($id = $services->save('services')){
                $_SESSION['success'] = 'Услуга добавлена';
            }
            redirect();
        }
        $this->setMeta('Новая услуга');
    }

    public function editAction(){
        if(!empty($_POST)){
            $id = $this->getRequestID(false);
            $services = new Services();
            $data = $_POST;
            $services->load($data);
            $services->attributes['status'] = $services->attributes['status'] ? '1' : '0';
            $services->getImg();
            if(!$services->validate($data)){
                $services->getErrors();
                redirect();
            }
            if($services->update('services', $id)){
                $_SESSION['success'] = 'Изменения сохранены';
                redirect();
            }
        }
        $id = $this->getRequestID();
        $services = R::load('services', $id);
        $this->setMeta("Редактирование товара {$services->title}");
        $this->set(compact('services'));
    }
    public function deleteAction(){

        $services_id = $this->getRequestID();
        $services = R::load('services', $services_id);
        R::trash($services);
        $_SESSION['success'] = 'Услуга удалена';
        redirect(ADMIN. '/services');
    }

}