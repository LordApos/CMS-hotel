<?php

namespace app\controllers\admin;


use RedBeanPHP\R;

class SuggestionController extends AppController{

    public function indexAction(){
        $suggs = R::findAll('suggest');
        $this->set(compact('suggs'));
        $this->setMeta('Список предложений');
    }
}