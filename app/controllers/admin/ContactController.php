<?php

namespace app\controllers\admin;


use app\models\admin\Contact;
use RedBeanPHP\R;

class ContactController extends AppController {

    public function indexAction(){
        $contacts = R::findAll('contact');
        $this->set(compact('contacts'));
        $this->setMeta('Список контактов');
    }
    public function addAction(){
        if(!empty($_POST)){
            $contact = new Contact();
            $data = $_POST;
            $contact->load($data);

            if(!$contact->validate($data)){
                $contact->getErrors();
                $_SESSION['form_data'] = $data;
                redirect();
            }

            if($id = $contact->save('contact')){
                $_SESSION['success'] = 'Контакт добавлен';
            }
            redirect();
        }
        $this->setMeta('Новый контакт');
    }
    public function editAction(){

        $contact_id = $this->getRequestID();
        $contact = R::load('contact', $contact_id);
        if(!empty($_POST)){
            $id = $this->getRequestID(false);
            $contact = new Contact();
            $data = $_POST;
            $contact->load($data);

            if(!$contact->validate($data)){
                $contact->getErrors();
                redirect();
            }
            if($contact->update('contact', $id)){
                $_SESSION['success'] = 'Изменения сохранены';
            }
            redirect();
        }
        $this->setMeta('Редактировать контакт');
        $this->set(compact('contact'));

    }
    public function deleteAction(){
        $contact_id = $this->getRequestID();
        $contact = R::load('contact', $contact_id);
        R::trash($contact);
        $_SESSION['success'] = 'Контакт удален';
        redirect(ADMIN. '/contact');
    }
}