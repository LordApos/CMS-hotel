<?php

namespace app\controllers\admin;


use app\models\admin\Room;
use app\models\AppModel;
use hotel\App;
use hotel\libs\Pagination;
use RedBeanPHP\R;

class RoomController extends AppController {

    public function indexAction(){
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = 5;
        $count = R::count('rooms');
        $pagination = new Pagination($page, $perpage, $count);
        $start = $pagination->getStart();
        $rooms = R::getAll("SELECT rooms.*, category.title AS cat FROM rooms JOIN category ON category.id = rooms.category_id ORDER BY rooms.title LIMIT $start, $perpage");
        $this->setMeta('Список номеров');
        $this->set(compact('rooms', 'pagination', 'count'));
    }

    public function addImageAction(){
        if(isset($_GET['upload'])){
            if($_POST['name'] == 'single'){
                $wmax = App::$app->getProperty('img_width');
                $hmax = App::$app->getProperty('img_height');
            }else{
                $wmax = App::$app->getProperty('gallery_width');
                $hmax = App::$app->getProperty('gallery_height');
            }
            $name = $_POST['name'];
            $room = new Room();
            $room->uploadImg($name, $wmax, $hmax);
        }
    }

    public function addAction(){
        if(!empty($_POST)){
            $room = new Room();
            $data = $_POST;
            $room->load($data);
            $room->attributes['status'] = $room->attributes['status'] ? '1' : '0';
            $room->attributes['hit'] = $room->attributes['hit'] ? '1' : '0';
            $room->getImg();

            if(!$room->validate($data)){
                $room->getErrors();
                $_SESSION['form_data'] = $data;
                redirect();
            }

            if($id = $room->save('rooms')){
                $room->saveGallery($id);
                $alias = AppModel::createAlias('rooms', 'alias', $data['title'], $id);
                $p = R::load('rooms', $id);
                $p->alias = $alias;
                R::store($p);
                $room->editRelatedRoom($id, $data);
                $_SESSION['success'] = 'Товар добавлен';
            }
            redirect();
        }

        $this->setMeta('Новый номер');
    }

    public function editRelatedRoomAction(){
        $q = isset($_GET['q']) ? $_GET['q'] : '';
        $data['items'] = [];
        $rooms = R::getAssoc('SELECT id, title FROM rooms WHERE title LIKE ? LIMIT 5', ["%{$q}%"]);
        if($rooms){
            $i = 0;
            foreach($rooms as $id => $title){
                $data['items'][$i]['id'] = $id;
                $data['items'][$i]['text'] = $title;
                $i++;
            }
        }
        echo json_encode($data);
        die;
    }

    public function editAction(){
        if(!empty($_POST)){
            $id = $this->getRequestID(false);
            $room = new Room();
            $data = $_POST;
            $room->load($data);
            $room->attributes['status'] = $room->attributes['status'] ? '1' : '0';
            $room->attributes['hit'] = $room->attributes['hit'] ? '1' : '0';
            $room->getImg();
            if(!$room->validate($data)){
                $room->getErrors();
                redirect();
            }
            if($room->update('rooms', $id)){
                $room->editRelatedRoom($id, $data);
                $room->saveGallery($id);
                $alias = AppModel::createAlias('rooms', 'alias', $data['title'], $id);
                $room = R::load('rooms', $id);
                $room->alias = $alias;
                R::store($room);
                $_SESSION['success'] = 'Изменения сохранены';
                redirect();
            }
        }
        $id = $this->getRequestID();
        $room = R::load('rooms', $id);
        App::$app->setProperty('parent_id', $room->category_id);
        $related_rooms = R::getAll("SELECT related_rooms.related_id, rooms.title FROM related_rooms JOIN rooms ON rooms.id = related_rooms.related_id WHERE related_rooms.room_id = ?", [$id]);
        $gallery = R::getCol('SELECT img FROM gallery WHERE room_id = ?', [$id]);
        $this->setMeta("Редактирование товара {$room->title}");
        $this->set(compact('room', 'filter', 'related_rooms', 'gallery'));
    }


    public function deleteGalleryAction(){
        $id = isset($_POST['id']) ? $_POST['id'] : null;
        $src = isset($_POST['src']) ? $_POST['src'] : null;
        if(!$id || !$src){
            return;
        }
        if(R::exec("DELETE FROM gallery WHERE room_id = ? AND img = ?", [$id, $src])){
            @unlink(WWW . "/images/$src");
            exit('1');
        }
        return;
    }

    public function deleteAction(){

        $room_id = $this->getRequestID();
        $room = R::load('rooms', $room_id);
        R::trash($room);
        $_SESSION['success'] = 'Номер удален';
        redirect(ADMIN. '/room');
    }
}
