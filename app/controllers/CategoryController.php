<?php
/**
 * Created by PhpStorm.
 * User: Apos
 * Date: 04.05.2018
 * Time: 14:32
 */

namespace app\controllers;


use app\models\Breadcrumbs;
use app\models\Category;
use hotel\App;
use hotel\libs\Pagination;
use http\Exception;
use RedBeanPHP\R;

class CategoryController extends AppController
{
    public function viewAction(){
        $alias = $this->route['alias'];
        $category = R::findOne('category', 'alias = ?', [$alias]);
        if (!$category){
            throw new \Exception('Страница не найдена', 404);
        }

        $breadcrumbs = Breadcrumbs::getBreadcrumbs($category->id);
        $cat_model = new Category();
        $ids = $cat_model->getIds($category->id);
        $ids = !$ids ? $category->id : $ids . $category->id;

        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = App::$app->getProperty('pagination');
        $total = R::count('rooms', "category_id IN ($ids)");
        $pagination = new Pagination($page, $perpage, $total);
        $start = $pagination->getStart();

        $rooms = R::find('rooms', "category_id IN ($ids) LIMIT $start, $perpage");
        $this->setMeta($category->title, $category->description, $category->keywords);
        $this->set(compact('rooms', 'breadcrumbs', 'pagination'));

    }
}