<?php
/**
 * Created by PhpStorm.
 * User: Apos
 * Date: 15.04.2018
 * Time: 15:58
 */

namespace app\controllers;


use app\models\Breadcrumbs;
use RedBeanPHP\R;

class RoomsController extends AppController
{
    public function viewAction(){
        $alias = $this->route['alias'];
        $room = R::findOne('rooms', "alias = ? AND status = '1'", [$alias]);
        if (!$room){
            throw new \Exception('Страница не найдена', 404);
        }
        /// xleb kroxi
        $breadcrumps = Breadcrumbs::getBreadcrumbs($room->category_id, $room->title);

        /// poxozie rooms
        $related = R::getAll("SELECT * FROM related_rooms JOIN rooms ON rooms.id = related_rooms.related_id WHERE related_rooms.room_id = ? LIMIT 3", [$room->id]);

        //gallery
        $gallery = R::findALL('gallery', 'room_id = ?', [$room->id]);


        $this->setMeta($room->title, $room->description, $room->keywords);
        $this->set(compact('room', 'related', 'gallery', 'breadcrumps'));


    }

}