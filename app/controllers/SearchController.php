<?php
/**
 * Created by PhpStorm.
 * User: Apos
 * Date: 04.05.2018
 * Time: 7:24
 */

namespace app\controllers;


use RedBeanPHP\R;

class SearchController extends AppController
{
    public function typeaheadAction(){
        if ($this->isAjax()){
            $query = !empty(trim($_GET['query'])) ? trim($_GET['query']) : null;
            if ($query){
                $rooms = R::getAll('SELECT id, title FROM rooms WHERE title LIKE ? LIMIT 5', ["%{$query}%"]);
                echo json_encode($rooms);
            }
        }
        die;
    }
    public function indexAction(){
        $query = !empty(trim($_GET['s'])) ? trim($_GET['s']) : null;
        if ($query){
            $rooms = R::find('rooms', "title LIKE ?", ["%{$query}%"]);
        }
        $this->setMeta('Поиск по: ' . h($query));
        $this->set(compact('rooms', 'query'));
    }
}