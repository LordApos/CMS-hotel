<?php
/**
 * Created by PhpStorm.
 * User: Apos
 * Date: 12.04.2018
 * Time: 18:41
 */

namespace app\controllers;



/*use hotel\Cache;
use RedBeanPHP\R;*/

use app\models\Suggestion;
use hotel\App;
use RedBeanPHP\R;

class MainController extends AppController
{

    public function indexAction(){
        $hits = R::find('rooms', "hit = '1' AND status = '1' LIMIT 3");
        $this->setMeta('Главная', 'DescHome', 'bestHotel');
        $this->set(compact( 'hits'));
    }
    public function contactAction(){
        $contacts  = R::findAll('contact');
        if (!empty($_POST)){
            $sugg = new Suggestion();
            $data = $_POST;
            $sugg->load($data);
            if (!$sugg->validate($data)){
                $sugg->getErrors();
                $_SESSION['form_data'] = $data;
            }else{
                if ($sugg->save('suggest')){
                    $_SESSION['success'] = 'Предложения доставленно';
                }else{
                    $_SESSION['error'] = 'Ошибка';
                }
            }
            redirect();
        }
        $this->setMeta('Контакты', 'DescHome', 'bestHotel');
        $this->set(compact( 'contacts'));
    }
    public function servicesAction(){
        $services = R::findAll('services', "status = '1'");
        $this->setMeta('Услуги', 'DescHome', 'bestHotel');
        $this->set(compact( 'services'));
    }
}