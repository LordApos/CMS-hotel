<!--banner-starts-->
<div class="bnr" id="home">
    <div  id="top" class="callbacks_container">
        <ul class="rslides" id="slider4">
            <li>
                <img src="images/bnr-1.jpg" alt=""/>
            </li>
            <li>
                <img src="images/bnr-2.jpg" alt=""/>
            </li>
            <li>
                <img src="images/bnr-3.jpg" alt=""/>
            </li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>
<!--banner-ends-->
<!--Slider-Starts-Here-->

<!--about-starts-->
<div class="about">
    <div class="container">
        <div class="about-top grid-1">
            <div class="col-md-4 about-left">
                <figure class="effect-bubba">
                    <img class="img-responsive" src="images/abt-1.jpg" alt=""/>
                    <figcaption>
                        <h2>Комфорт</h2>
                        <p>Гостиница, в которой хочется остаться!</p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-4 about-left">
                <figure class="effect-bubba">
                    <img class="img-responsive" src="images/abt-2.jpg" alt=""/>
                    <figcaption>
                        <h4>Сервис</h4>
                        <p>Вам у нас понравится!</p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-4 about-left">
                <figure class="effect-bubba">
                    <img class="img-responsive" src="images/abt-3.jpg" alt=""/>
                    <figcaption>
                        <h4>Цена</h4>
                        <p>Тепло и уют по комфортной цене.</p>
                    </figcaption>
                </figure>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--about-end-->
<!--product-starts-->
<?php if ($hits): ?>
<?php $curr = \hotel\App::$app->getProperty('currency');?>
<div class="product">
    <div class="container">
        <div class="product-top">
            <div class="product-one">
                <?php foreach ($hits as $hit): ?>
                <div class="col-md-4 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="rooms/<?=$hit->alias;?>" class="mask"><img class="img-responsive zoom-img" src="images/<?=$hit->img;?>" alt="" /></a>
                        <div class="product-bottom">
                            <h3><a href="rooms/<?=$hit->alias;?>"><?=$hit->title;?></a></h3>
                            <h4>
                                <a data-id="<?=$hit->id;?>" class="add-to-cart-link" href="cart/add?id=<?=$hit->id;?>"><i></i></a> <span class=" item_price"><?=$curr['symbol_left'];?> <?=$hit->price * $curr['value'];?><?=$curr['symbol_right'];?></span>
                                <?php if ($hit->old_price !=0): ?>
                                <small><del><?=$curr['symbol_left'];?><?=$hit->old_price * $curr['value'];;?><?=$curr['symbol_right'];?></del></small>
                                <?php endif;?>
                            </h4>
                        </div>
                        <?php if ($hit->old_price !=0): ?>
                        <div class="srch">
                            <span><?php
                            $diss = 100 - ($hit->price * 100 / $hit->old_price);
                            echo $diss . '%';
                                ?></span>
                        </div>
                        <?php endif;?>

                    </div>
                </div>
                <?php endforeach;?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
<!--product-end-->