<!--start-breadcrumbs-->
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs-main">
            <ol class="breadcrumb">
                <li><a href="<?=PATH?>">Главная</a></li>
                <li class="active">Услуги</li>
            </ol>
        </div>
    </div>
</div>
<!--end-breadcrumbs-->
<!--start-single-->
<div class="single contact">
    <div class="container">
        <?php foreach ($services as $service): ?>
        <div class="single-main">
            <div class="col-md-12">
                <div class="sngl-top">
                    <div class="col-md-4 single-top-left">
                        <img src="images/<?=$service->img;?>" alt="">
                    </div>
                    <div class="col-md-7 single-top-right">
                        <div class="single-para simpleCart_shelfItem">
                            <h2><?=$service->title;?></h2>
                            <p><?=$service->text;?></p>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
            <br>
        <?php endforeach;?>
    </div>
</div>
<!--end-single-->