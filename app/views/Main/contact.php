<!--start-breadcrumbs-->
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs-main">
            <ol class="breadcrumb">
                <li><a href="<?=PATH?>">Главная</a></li>
                <li class="active">Контакты</li>
            </ol>
        </div>
    </div>
</div>
<!--end-breadcrumbs-->
<!--contact-start-->
<div class="contact">
    <div class="container">
        <div class="contact-top heading">
            <h2>Контакты</h2>
        </div>
        <div class="contact-text">
            <?php foreach ($contacts as $contact): ?>
            <div class="col-md-3 col-sm-6 contact-left">
                <div class="address">
                    <h5><?=$contact->title;?></h5>
                    <?=$contact->text;?>
                </div>
            </div>
            <?php endforeach;?>
            <div class="clearfix"></div>
        </div>

        <div class="col-md-12">
            <div class="product-one signup">
                <div class="register-top heading">
                    <h2>Форма обратной связи</h2>
                </div>
                <div class="register-main">
                    <div class="col-md-12 account-left">
                        <form method="post" action="main/suggestion" id="signup" role="form" data-toggle="validator">
                            <div class="form-group has-feedback">
                                <label for="sugg">Предложения</label>
                                <textarea name="text" class="form-control" id="sugg" required><?=isset($_SESSION['form_data']['name']) ? h($_SESSION['form_data']['text']) : '';?></textarea>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 account-left">
                                <div class="form-group has-feedback">
                                    <label for="name">Имя</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Имя" value="<?=isset($_SESSION['form_data']['name']) ? h($_SESSION['form_data']['name']) : '';?>" required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="col-md-6 account-right">
                                <div class="form-group has-feedback">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="<?=isset($_SESSION['form_data']['email']) ? h($_SESSION['form_data']['email']) : '';?>" required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-default">Предложить</button>
                        </form>
                        <?php if (isset($_SESSION['form_data'])) unset($_SESSION['form_data']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--contact-end-->
<!--map-start-->
<div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d5081.203556770549!2d30.5228486260415!3d50.44851774972888!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x40d4ce53dbbdc391%3A0xd4a2181f3a7cfcbe!2z0LLRg9C70LjRhtGPINCG0L3RgdGC0LjRgtGD0YLRgdGM0LrQsCwgNCwg0JrQuNGX0LIsINCj0LrRgNCw0LjQvdCwLCAwMjAwMA!3m2!1d50.448510999999996!2d30.527226!5e0!3m2!1sru!2sus!4v1526220775580"></iframe>
</div>
<!--map-end-->