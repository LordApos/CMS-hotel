<!--start-breadcrumbs-->
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs-main">
            <ol class="breadcrumb">
                <li><a href="<?=PATH?>">Главная</a></li>
                <li>Поиск по запросу "<?=h($query);?>"</li>
            </ol>
        </div>
    </div>
</div>
<!--end-breadcrumbs-->
<!--prdt-starts-->
<div class="prdt">
    <div class="container">
        <div class="prdt-top">
            <div class="col-md-12 prdt-left">
                <?php if (!empty($rooms)): ?>
                    <?php $curr = \hotel\App::$app->getProperty('currency');?>
                <div class="product-one">
                    <?php foreach ($rooms as $room) : ?>
                        <div class="col-md-4 product-left p-left">
                        <div class="product-main simpleCart_shelfItem">
                            <a href="rooms/<?=$room->alias;?>" class="mask"><img class="img-responsive zoom-img" src="images/<?=$room->img;?>" alt="" /></a>
                            <div class="product-bottom">
                                <h3><?=$room->title;?></h3>
                                <h4>
                                    <a data-id="<?=$room->id;?>" class="add-to-cart-link" href="cart/add?id=<?=$room->id;?>"><i></i></a> <span class=" item_price"><?=$curr['symbol_left'];?> <?=$room->price * $curr['value'];?><?=$curr['symbol_right'];?></span>
                                    <?php if ($room->old_price !=0): ?>
                                        <small><del><?=$curr['symbol_left'];?><?=$room->old_price * $curr['value'];;?><?=$curr['symbol_right'];?></del></small>
                                    <?php endif;?>
                                </h4>
                            </div>
                            <?php if ($room->old_price !=0): ?>
                            <div class="srch">
                            <span><?php
                                $diss = 100 - ($room->price * 100 / $room->old_price);
                                echo $diss . '%';
                                ?></span>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php endforeach;?>
                    <div class="clearfix"></div>
                </div>
                <?php endif; ?>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--product-end-->