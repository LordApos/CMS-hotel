<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Новая услуга</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN?>">Главная</a></li>
                    <li class="breadcrumb-item"><a href="<?=ADMIN?>/services">Список услуг</a></li>
                    <li class="breadcrumb-item active">Новая услуга</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <form action="<?=ADMIN;?>/services/edit" method="post" data-toggle="validator">
                        <div class="box-body">
                            <div class="form-group has-feedback">
                                <label for="title">Наименование услуги</label>
                                <input type="text" name="title" class="form-control" id="title" placeholder="Наименование услуги" value="<?=$services->title?>" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="content">Контент</label>
                                <textarea name="text" id="editor1" cols="145" rows="10"><?=$services->text?></textarea>
                            </div>

                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="status" <?php if ($services->status == '1'){ echo 'checked';}?>> Статус
                                </label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="card card-danger card-solid file-upload">
                                        <div class="card-header">
                                            <h3 class="card-title">Базовое изображение</h3>
                                        </div>
                                        <div class="card-body">
                                            <div id="single" class="btn btn-success" data-url="room/add-image" data-name="single">Выбрать файл</div>
                                            <p><small>Рекомендуемые размеры: 280х250</small></p>
                                            <div class="single">
                                                <img src="/images/<?=$services->img;?>" alt="" style="max-height: 150px;">
                                            </div>
                                        </div>
                                        <div class="overlay">
                                            <i class="fa fa-refresh fa-spin"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <input type="hidden" name="id" value="<?=$services->id;?>">
                            <button type="submit" class="btn btn-success">Редактировать</button>
                        </div>
                    </form>
                    <?php if(isset($_SESSION['form_data'])) unset($_SESSION['form_data']); ?>
                </div>
            </div>
        </div>
    </div>
</section>