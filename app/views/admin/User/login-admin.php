<div class="login-box">
    <div class="login-logo">
        <b>Admin <i style="color: red">Hotel</i></b>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Войдите, чтобы начать сеанс</p>

            <?php if (isset($_SESSION['error'])):?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-ban"></i> Ошибка!</h5>
                    <?=$_SESSION['error']; unset($_SESSION['error']);?>
                </div>
            <?php endif;?>

            <form action="<?=ADMIN;?>/user/login-admin" method="post">
                <div class="form-group has-feedback">
                    <input name="login" type="text" class="form-control" placeholder="Логин">
                </div>
                <div class="form-group has-feedback">
                    <input name="password" type="password" class="form-control" placeholder="Пароль">
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->