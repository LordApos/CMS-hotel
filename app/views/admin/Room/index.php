<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Список номеров</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=ADMIN?>">Главная</a></li>
                    <li class="breadcrumb-item active">Список номеров</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Категория</th>
                                        <th>Наименование</th>
                                        <th>Цена</th>
                                        <th>Статус</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($rooms as $room): ?>
                                        <tr>
                                            <td><?=$room['id'];?></td>
                                            <td><?=$room['cat'];?></td>
                                            <td><?=$room['title'];?></td>
                                            <td><?=$room['price'];?></td>
                                            <td><?=$room['status'] ? 'On' : 'Off';?></td>
                                            <td><a href="<?=ADMIN;?>/room/edit?id=<?=$room['id'];?>"><i class="fa fa-fw fa-edit"></i></a> <a class="delete" href="<?=ADMIN;?>/room/delete?id=<?=$room['id'];?>"><i class="fa fa-fw fa-close text-danger"></i></a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <p>(<?=count($rooms);?> заказа(ов) из <?=$count;?>)</p>
                                    <?php if ($pagination->countPages > 1) : ?>
                                        <?=$pagination;?>
                                    <?php endif;?>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>