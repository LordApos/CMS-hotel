<!--start-breadcrumbs-->
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs-main">
            <ol class="breadcrumb">
                <?=$breadcrumps;?>
            </ol>
        </div>
    </div>
</div>
<!--end-breadcrumbs-->
<!--start-single-->
<div class="single contact">
    <div class="container">
        <div class="single-main">
            <div class="col-md-12">
                <div class="sngl-top">
                    <div class="col-md-4 single-top-left">
                        <?php if ($gallery): ?>
                        <div class="flexslider col-md-11">
                            <ul class="slides">
                                <?php foreach ($gallery as $item): ?>
                                <li data-thumb="images/<?=$item->img;?>">
                                    <div class="thumb-image"> <img src="images/<?=$item->img;?>" class="img-responsive" alt=""/> </div>
                                </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                            <?php else: ?>
                            <img src="images/<?=$room->img;?>" alt="">
                        <?php endif;?>

                    </div>
                    <?php
                    $curr = \hotel\App::$app->getProperty('currency');
                    $cats = \hotel\App::$app->getProperty('cats');
                    ?>
                    <div class="col-md-8 single-top-right">
                        <div class="single-para simpleCart_shelfItem">
                            <h2><?=$room->title;?></h2>
                            <!--<div class="star-on">
                                <ul class="star-footer">
                                    <li><a href="#"><i> </i></a></li>
                                    <li><a href="#"><i> </i></a></li>
                                    <li><a href="#"><i> </i></a></li>
                                    <li><a href="#"><i> </i></a></li>
                                    <li><a href="#"><i> </i></a></li>
                                </ul>
                                <div class="review">
                                    <a href="#"> 1 customer review </a>
                                </div>
                                <div class="clearfix"> </div>
                            </div>-->

                            <h5 class="item_price"><?=$curr['symbol_left'];?> <?=$room->price * $curr['value'];?><?=$curr['symbol_right'];?>
                                <?php if ($room->old_price !=0): ?>
                                <small>
                                    <del>
                                        <?=$curr['symbol_left'];?>
                                            <?=$room->old_price * $curr['value'];?>
                                        <?=$curr['symbol_right'];?>
                                    </del>
                                </small>
                                <?php endif;?>
                            </h5>
                           <p><?=$room->content;?></p>

                            <ul class="tag-men">
                                <li><span>Category</span>
                                    <span><a href="category/<?=$cats[$room->category_id]['alias']; ?>">
                                            <?=$cats[$room->category_id]['title']; ?>
                                        </a></span></li>
                            </ul>
                            <div class="quantity">
                                <input type="number" size="4" value="1" name="quantity" min="1" step="1">
                            </div>
                            <a id="roomAdd" data-id="<?=$room->id;?>" href="<?=PATH?>/cart/add?id=<?=$room->id;?>" class="add-cart item_add add-to-cart-link">Добавить в корзину</a>

                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <?php if ($related): ?>
                <div class="latestproducts">
                    <div class="product-one">
                        <h3>Похожие номера:</h3>
                        <?php foreach ($related as $item): ?>
                        <div class="col-md-4 product-left p-left">
                            <div class="product-main simpleCart_shelfItem">
                                <a href="rooms/<?=$item['alias'];?>" class="mask"><img class="img-responsive zoom-img" src="images/<?=$item['img'];?>" alt="" /></a>
                                <div class="product-bottom">
                                    <h3><a href="rooms/<?=$item['alias'];?>"><?=$item['title'];?></a></h3>
                                    <h4><a class="item_add add-to-cart-link" href="cart/add?id=<?=$item['id'];?>" data-id="<?=$item['id'];?>"><i></i></a>
                                        <span class=" item_price">
                                            <?=$curr['symbol_left'];?> <?=$item['price'] * $curr['value'];?><?=$curr['symbol_right'];?>
                                            <?php if ($item['old_price'] !=0): ?>
                                                <small>
                                    <del>
                                        <?=$curr['symbol_left'];?>
                                        <?=$item['old_price'] * $curr['value'];?>
                                        <?=$curr['symbol_right'];?>
                                    </del>
                                </small>
                                            <?php endif;?>
                                        </span>
                                    </h4>
                                </div>
                                <?php if ($item['old_price'] !=0): ?>
                                    <div class="srch">
                            <span><?php
                                $diss = 100 - ($item['price'] * 100 / $item['old_price']);
                                echo $diss . '%';
                                ?></span>
                                    </div>
                                <?php endif;?>
                            </div>
                        </div>
                        <?php endforeach;?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php endif;?>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--end-single-->