<?php

namespace app\models;


use hotel\App;
use RedBeanPHP\R;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class Order extends AppModel
{
    public static function saveOrder($data){
        $order = R::dispense('order');
        $order->user_id = $data['user_id'];
        $order->note = $data['note'];
        $order->currency = $_SESSION['cart.currency']['code'];
        $datestart = $data['datestart'];
        $datefinish = $data['datefinish'];
        foreach ($_SESSION['cart'] as $room_id => $room){
            $room_id = (int)$room_id;
            $roomlocks = R::find('order_room',"room_id = ?", [$room_id]);
            $findroom = [];
            foreach ($roomlocks as $roomlock){
                $days = getDates($datestart, $datefinish);
                $dayslock = getDates($roomlock->datestart, $roomlock->datefinish);
                    for ($i = 0; $i <= count($roomlocks); $i++){
                        if (in_array($dayslock[$i], $days)){
                            $findroom[$i] += 1;
                        }
                        else{
                            $findroom[$i] += 0;
                        }
                    }
                }
        }
        if (empty($findroom)){
            $order_id = R::store($order);
            self::saveOrderRoom($order_id, $datestart, $datefinish);
            return $order_id;
        } else {
            $_SESSION['error'] = 'Ошибка. Данный номер(а) забронирован(ые) на эту дату';
            return false;
        }
    }

    public static function saveOrderRoom($order_id, $datestart, $datefinish){
        $sql_part = '';
        foreach ($_SESSION['cart'] as $room_id => $room){
            $room_id = (int)$room_id;
            $sql_part .= "($order_id, $room_id, {$room['qty']}, '{$room['title']}', {$room['price']}, '{$datestart}', '{$datefinish}'),";
        }
        $sql_part = rtrim($sql_part, ',');
        R::exec("INSERT INTO order_room(order_id, room_id, qty, title, price, datestart, datefinish) VALUES $sql_part");
    }

    public static function mailOrder($order_id, $user_email){
        $transport = (new Swift_SmtpTransport(App::$app->getProperty('smtp_host'), App::$app->getProperty('smtp_port'), App::$app->getProperty('smtp_protocol')))
            ->setUsername(App::$app->getProperty('smtp_login'))
            ->setPassword(App::$app->getProperty('smtp_password'))
        ;
        $mailer = new Swift_Mailer($transport);

        //Смс
        ob_start();
        require APP . '/views/mail/mail_order.php';
        $body = ob_get_clean();

        $message_client = (new Swift_Message("Ваша бронь №{$order_id}"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('hotel_name')])
            ->setTo($user_email)
            ->setBody($body, 'text/html')
        ;
        $message_admin = (new Swift_Message("Сделана бронь №{$order_id}"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('hotel_name')])
            ->setTo(App::$app->getProperty('admin_email'))
            ->setBody($body, 'text/html')
        ;
        $result = $mailer->send($message_client);
        $result = $mailer->send($message_admin);
        unset($_SESSION['cart']);
        unset($_SESSION['cart.qty']);
        unset($_SESSION['cart.sum']);
        unset($_SESSION['cart.currency']);
        $_SESSION['success'] = 'Спасибо за Вашу бронь. В ближайшее время с Вами свяжется менеджер для согласования заказа.';

    }

}