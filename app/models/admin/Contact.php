<?php

namespace app\models\admin;


use app\models\AppModel;
use RedBeanPHP\R;

class Contact extends AppModel {

    public $attributes = [
        'title' => '',
        'text' => '',
    ];

    public $rules = [
        'required' => [
            ['title'],
            ['text'],
        ],
    ];
}