<?php
/**
 * Created by PhpStorm.
 * User: Apos
 * Date: 13.04.2018
 * Time: 19:39
 */

namespace app\models;


use hotel\base\Model;
use RedBeanPHP\R;

class AppModel extends Model
{

    public static function createAlias($table, $field, $str, $id) {
        $str = self::str2url($str);
        $res = R::findOne($table, "$field = ?", [$str]);
        if ($res){
            $str = "{$str}-{$id}";
            $res = R::count($table, "$field = ?", [$str]);
            if ($res){
                $str = self::createAlias($table, $field, $str, $id);
            }
        }
        return $str;
    }

    public static function str2url($str){
        $str = self::rus2translit($str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        $str = trim($str, "-");
        return $str;
    }
    public static function rus2translit($string){

        $converter = array(
            'а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z',
            'и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r',
            'с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch',
            'ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'\'','ь'=>'\'','А'=>'a','Б'=>'b','В'=>'v',
            'Г'=>'g','Д'=>'d','Е'=>'e','Ё'=>'e','Ж'=>'j','З'=>'z',
            'И'=>'i','Й'=>'y','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r',
            'С'=>'s','Т'=>'t','У'=>'u','Ф'=>'f','Х'=>'h','Ц'=>'c','Ч'=>'ch','Ш'=>'sh','Щ'=>'shch',
            'Ы'=>'y','Э'=>'e','Ю'=>'yu','Я'=>'ya','Ъ'=>'\'','Ь'=>'\'','і'=>'i','І'=>'i','ї'=>'ii','Ї'=>'ii'
        );
        return strtr($string,$converter);

    }
}