<?php

namespace app\models;


class Suggestion extends AppModel {

    public $attributes  = [

        'text' => '',
        'name' => '',
        'email' => '',

    ];

    public $rules = [
        'required' => [
            ['text'],
            ['name'],
            ['email'],
        ],
        'email' => [
            ['email'],
        ],
    ];

}