-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 07 2018 г., 10:38
-- Версия сервера: 5.6.31
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `hotel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `parent_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `title`, `alias`, `parent_id`, `keywords`, `description`) VALUES
(1, 'Одноместное размещение', 'single', 9, NULL, NULL),
(2, 'Двухместное размещение', 'double ', 9, 'двухместное размещение', 'двухместное размещение'),
(3, 'Трехместное размещение', 'triple ', 9, 'трехместное размещение', 'трехместное размещение'),
(9, 'Номера', 'rooms', 0, 'Номера', 'Номера');

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`id`, `title`, `text`) VALUES
(2, 'Отдел бронирования', '<p><strong>Телефон:</strong> +380 44 278 28 04</p>\r\n\r\n<p><strong>Телефон:</strong> +380 44 590 44 00</p>\r\n'),
(3, 'Служба маркетинга', '<p><strong>Телефон:</strong> +380 44 590 54 74</p>\r\n\r\n<p><strong>Телефон:</strong> +380 44 590 44 00</p>\r\n'),
(4, 'Конференции и банкеты', '<p><strong>Ресторан:</strong> +380 44 279 19 67</p>\r\n\r\n<p><strong>Конференц-залы:</strong> +380 44 590 44</p>\r\n'),
(5, 'Гостиница Camelot', '<p><strong>ул. Институтская 4, г. Киев</strong></p>\r\n\r\n<p><strong>Тел.:</strong> +380 44 279 03 47</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(10) NOT NULL,
  `symbol_right` varchar(10) NOT NULL,
  `value` float(15,2) NOT NULL,
  `base` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `currency`
--

INSERT INTO `currency` (`id`, `title`, `code`, `symbol_left`, `symbol_right`, `value`, `base`) VALUES
(1, 'гривна', 'UAH', '', ' грн.', 26.00, '0'),
(2, 'доллар', 'USD', '$ ', '', 1.00, '1'),
(3, 'евро', 'EUR', '€ ', '', 0.89, '0');

-- --------------------------------------------------------

--
-- Структура таблицы `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery`
--

INSERT INTO `gallery` (`id`, `room_id`, `img`) VALUES
(4, 2, '9de4e504c3fcf23888f9d9f28c357f2b.jpg'),
(5, 2, '288eaeddf1163d0269345ba697b0c4a7.jpg'),
(6, 2, '0be95b0875f493962ff54aec2b4b2c2e.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL,
  `currency` varchar(10) NOT NULL,
  `note` text
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `user_id`, `status`, `date`, `update_at`, `currency`, `note`) VALUES
(102, 5, '0', '2018-05-21 09:40:54', NULL, 'USD', ''),
(103, 5, '0', '2018-06-04 13:28:33', NULL, 'USD', '');

-- --------------------------------------------------------

--
-- Структура таблицы `order_room`
--

CREATE TABLE IF NOT EXISTS `order_room` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `datestart` date NOT NULL,
  `datefinish` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_room`
--

INSERT INTO `order_room` (`id`, `order_id`, `room_id`, `qty`, `title`, `price`, `datestart`, `datefinish`) VALUES
(30, 102, 2, 1, 'Делюкс', 320, '2018-05-21', '2018-05-31'),
(31, 103, 1, 1, 'Классик', 270, '2018-06-04', '2018-06-06');

-- --------------------------------------------------------

--
-- Структура таблицы `related_rooms`
--

CREATE TABLE IF NOT EXISTS `related_rooms` (
  `room_id` int(10) unsigned NOT NULL,
  `related_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `related_rooms`
--

INSERT INTO `related_rooms` (`room_id`, `related_id`) VALUES
(1, 2),
(1, 3),
(2, 3),
(2, 1),
(3, 2),
(3, 1),
(4, 3),
(4, 2),
(5, 6),
(5, 7),
(6, 5),
(6, 7),
(7, 6),
(7, 5),
(1, 4),
(2, 4),
(3, 4),
(4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(10) unsigned NOT NULL,
  `category_id` tinyint(3) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `content` text,
  `price` float NOT NULL DEFAULT '0',
  `old_price` float NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `img` varchar(255) NOT NULL DEFAULT 'no_image.jpg',
  `hit` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `rooms`
--

INSERT INTO `rooms` (`id`, `category_id`, `title`, `alias`, `content`, `price`, `old_price`, `status`, `keywords`, `description`, `img`, `hit`) VALUES
(1, 1, 'Классик', 'Classik', 'Просторный номер площадью 30 кв.м. сочетает в себе все необходимое для эффективной работы и приятного отдыха. Насладитесь неограниченным доступом к бесплатной сети Wi-Fi. Выбор подушек, бесплатный минибар, набор для приготовления чая/кофе придадут дополнительный комфорт Вашему пребыванию. Желаемый тип кровати (кинг или твин) будет предоставлен по Вашему запросу в зависимости от наличия.', 270, 300, '1', NULL, NULL, 'p-1.png', '1'),
(2, 1, 'Делюкс', 'delyuks', '<p>Насладитесь уютом и прекрасным видом на город в номере Делюкс площадью 32 кв.м. В Вашем распоряжении бесплатное парковочное место. Бесплатный минибар, наборы для приготовления чая/кофе, Ваши любимые телепрограммы на экране с диагональю 36 дюймов и iPod-станция создадут для Вас незабываемую атмосферу домашнего уюта.</p>\r\n', 320, 0, '1', '', '', 'p-2.png', '1'),
(3, 2, 'Клаб Делюкс', 'clubdelux', 'Наслаждайтесь уютом и комфортом в номере площадью 32 кв.м., а также захватывающими видами Киева с верхних этажей отеля. Почувствуйте преимущества доступа в Club InterContinental, оборудованного по последнему слову техники бизнес-центра и комнаты для переговоров на втором этаже отеля. Бесплатный Wi-Fi, освежающие напитки, закуски в течение дня и открытый алкогольный бар вечером - это все, что нужно для идеального бизнес-путешествия.', 340, 0, '1', NULL, NULL, 'p-3.png', '1'),
(4, 2, 'Клаб Экзекьютив', 'clubexequtiv', 'В просторном номере общей площадью 52 кв.м. есть все необходимое для ощущения полного комфорта. Мы предоставим Вам великолепную возможность  насладится прекрасными видами города, также воспользоваться преимуществами доступа в Club InterContinental, оборудованного по последнему слову техники бизнес-центра и комнаты для переговоров на втором этаже отеля. Бесплатный Wi-Fi, освежающие напитки, закуски в течение дня и открытый алкогольный бар вечером - это все, что нужно для идеального бизнес-путешествия.', 306, 360, '1', NULL, NULL, 'p-4.png', '1'),
(5, 2, 'Посольские Апартаменты', 'posolapparts', 'Из окон роскошных Посольских Апартаментов, общей площадью 70 кв.м, открываются завораживающие панорамы центра города. Номер сочетает в себе все необходимое для эффективной работы и приятного отдыха. Гостиная, спальня, роскошная ванная комната приятно удивят даже самых притязательных ценителей роскоши и комфорта. С эксклюзивным доступом в Club InterContinental Вы сможете насладиться вкусным завтраком в уединенной атмосфере.', 500, 0, '1', NULL, NULL, 'p-5.png', '0'),
(6, 3, 'Губернаторские Апартаменты', 'gubernatorapparts', 'Насладитесь захватывающим дух видом Киева из наших Губернаторских апартаментов площадью 85 кв.м., которые расположены на верхнем этаже отеля. Полные солнечного света, две уютные комнаты с французскими балконами позволят Вам в полной мере насладиться уникальными видами города. Эксклюзивный доступ в Club InterContinental, бесплатный Wi-Fi и мини-бар придадут дополнительный комфорт Вашему пребыванию.', 600, 0, '1', NULL, NULL, 'p-6.png', '0'),
(7, 3, 'Королевские Апартаменты', 'kingapparts', 'Пребывание в наших Королевских апартаментах площадью 150 кв.м. позволит вам оценить удивительный вид на Михайловскую площадь и золотые купола Софийского собора. Апартаменты идеально подойдут для частных и деловых встреч, а также для комфортного и приятного семейного отдыха. Бесплатный минибар и беспроводной интернет, а также VIP комплименты от отеля придадут дополнительный комфорт вашему пребыванию. Смежный номер категории «Клаб Экзекьютив» может быть предоставлен по запросу.', 650, 0, '1', NULL, NULL, 'p-7.png', '0'),
(8, 3, 'Президентские Апартаменты', 'presidentapparts', 'Почувствуйте изысканную атмосферу роскоши в одном из лучших Президентских апартаментов Европы площадью 275 кв.м. Вам откроются невероятные виды на историческую часть города.  Просторная спальня,  прямой доступ к гостевому лифту, столовая и кухня,  отдельная комната для охраны приятно удивят даже самых притязательных ценителей роскоши и комфорта. Смежный номер категории «Клаб Делюкс» может быть предоставлен по запросу.', 800, 0, '1', NULL, NULL, 'p-8.png', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`id`, `title`, `text`, `img`, `status`) VALUES
(15, 'Специальное предложение для молодоженов', '<p>Каждая история любви имеет свое неповторимое очарование. А мы стараемся добавить к нему еще больше восхитительных вкусных моментов. Наслаждайтесь специальным предложением для молодоженов из нашего Luxury Amenity Boutique - шампанское Veuve Clicquot с клубникой в черном и белом шоколаде по специальной цене.</p>\r\n\r\n<p><strong>Больше информации:&nbsp;+38 044 219 1919.</strong></p>\r\n', 'd0d738e8b96057d302d6c804756d51e1.jpg', '1'),
(16, 'Бизнес-ланч в ресторане Comme II Faut', '<p>Ресторан Comme Il Faut рекомендует Вам&nbsp;насладиться одним из лучших бизнес-ланчей&nbsp;в Киеве. Ланч-меню ресторана предлагает&nbsp;симбиоз рецептов французской и украинской&nbsp;кухни с прекрасной подборкой сырного, мясного ассорти, свежих салатов и овощей,&nbsp;а также супов и основных блюд.&nbsp;</p>\r\n\r\n<p>Пн-Пт, 12:00-15:00*</p>\r\n\r\n<p>*По выходным, а также в праздничные дни, предложения из бизнес-ланч меню не действуют</p>\r\n', '4144bd2eca98368c2c60ddd96171191f.jpg', '1'),
(17, 'Суши меню в Lobby Lounge Bar', '<p>Наслаждайтесь авторскими роллами от суши-шефа b-hush теперь и в Lobby Lounge Bar.</p>\r\n\r\n<p>Меню доступно каждый день, с 12:00 до 00:00.</p>\r\n\r\n<p><a href="http://media.kiev.intercontinental.com/d/intercontinental/media/Lobbybar/sushi_menu_inside.pdf">Посмотреть меню</a></p>\r\n\r\n<p><strong>Больше информации:&nbsp;+38 044 219 1919.</strong></p>\r\n', '26cfededaf4c112beb6a3ea389c28f98.jpg', '1'),
(18, 'Послеполуденный Чай', '<p>Наслаждайтесь традиционным Послеполуденным Чаем с большим выбором разнообразной домашней выпечки, сэндвичей и канапе с чаем на ваш выбор из чайной коллекции&nbsp;TWG.&nbsp; &nbsp;</p>\r\n\r\n<p><a href="http://media.intercontinental-kiev.com/d/intercontinental/media/Menu_lobby_UKR_print.pdf" target="_blank">Посмотреть меню</a></p>\r\n', '3cdef761caff4bcaa0912014e4ed0535.jpg', '1'),
(19, 'Бизнес-завтрак в приватной атмосфере', '<p>Утрення встреча с бизнес партнерами или друзьями может стать настоящим удовольствием с нашим предложением завтраков в приватной комнате.</p>\r\n\r\n<p>Наслаждайтесь мясным и рыбным ассорти, сырной нарезкой, фруктовым салатом и йогуртом, ароматными круасанами и выпечкой, яйцами на ваш выбор, а так же чаем и кофе без ограничений в специальной приватной зоне завтрака.</p>\r\n\r\n<p>Каждое утро &nbsp;с 6:30 до 10:30 утра.</p>\r\n\r\n<p>Предварительное бронирование обязательно.</p>\r\n', '1c9dcb517e3064847b2de2305ad6c95a.jpg', '1'),
(20, 'Детское меню', '<p>Создание специального детского меню было доверено детскому диетологу, популярному автору и кавалеру Ордена Британской Империи -&nbsp;<strong>Аннабель Кармель</strong>&nbsp;и обладателю множества премий &ndash; шеф-повару&nbsp;<strong>Тео Рэнделлу.</strong></p>\r\n\r\n<p>Новое детское меню порадует маленьких гурманов многообразием всеми любимых блюд и новых рецептов, отражающих культурное и кулинарное разнообразие стран, в которых расположены отели&nbsp;InterContinental.</p>\r\n\r\n<p><strong>Мы предлагаем<a href="http://kyiv.intercontinental.com/d/intercontinental/media/CIF/Children_menu_UKR.pdf" target="_blank">&nbsp;детское меню</a>&nbsp;во всех ресторанах отеля.</strong></p>\r\n', '1aa797fa1850ca688f7b94e2857d3c74.jpg', '1'),
(21, 'Город и его секреты', '<p><strong>InterContinental Hotels &amp; Resorts&reg;</strong>&nbsp; представил новую программу Insider Experience, которая раскроет секреты города.</p>\r\n\r\n<p>The Insider Experience является частью обязательств InterContinental Hotels &amp; Resorts по предоставлению гостям эксклюзивных преимуществ, которые помогают раскрыть секреты города.</p>\r\n\r\n<p>InterContinental Hotels &amp; Resorts предлагает инновационный и запоминающийся гастрономический опыт и рекомендации своим гостям, предлагая Мишленовские рестораны, лучшие бары и шеф-поваров с мировым именем.</p>\r\n\r\n<p>Откройте для себя нерассказанные истории благодаря эксклюзивной программе&nbsp;<strong>Insider Experiences</strong>, запущенной в Лондоне, Париже и Тель-Авиве. &nbsp;&nbsp;</p>\r\n', '4d74480936c54a955f1294857db67f92.jpg', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `suggest`
--

CREATE TABLE IF NOT EXISTS `suggest` (
  `id` int(10) NOT NULL,
  `text` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `suggest`
--

INSERT INTO `suggest` (`id`, `text`, `name`, `email`) VALUES
(1, 'тест', 'тест', 'user1@mail.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `role` enum('user','admin') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `email`, `name`, `address`, `role`) VALUES
(2, 'user1', 'qweqwe', 'user1@mail.ru', 'user1', 'qwert', 'user'),
(3, 'user2', '$2y$10$SN1QlvrAWMQQalHDpLiLfuDLSlBL7BJo8KbSb/1hNJLHOYkI7MWI6', 'user2@mail.ru', 'user2', 'qwert', 'user'),
(4, 'user4', '$2y$10$pyZxVtsFtdWemWz2Lx074Orv9P60mkuyr0TxY8OY700MoV9dkmkEa', 'user4@mail.ru', 'user4', 'qwert', 'user'),
(5, 'admin', '$2y$10$TyAU7ApPSIjOl2t2s2lrmek.wIFtVpaidlvMBW39GK8BfHTzQiEZO', 'lordaposs@gmail.com', 'admin', 'Ukraine', 'admin'),
(6, 'qwerty', '$2y$10$VVQsANQxaYCFo.vZ7mj4a.4vD3L1viI47eUAfuKShvqHqvY92k/2K', 'qwertyu@mail.ru', 'qwer', 'qwert', 'user'),
(7, 'LordApos', '$2y$10$TvHJIqHRf5cP8RPprX3NCO2OwU9RTE13WFkXr602ogTMvI3NFINzi', 'hellishapos@gmail.com', 'Богдан', 'Ukraine23', 'admin');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_room`
--
ALTER TABLE `order_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `suggest`
--
ALTER TABLE `suggest`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT для таблицы `order_room`
--
ALTER TABLE `order_room`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `suggest`
--
ALTER TABLE `suggest`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `order_room`
--
ALTER TABLE `order_room`
  ADD CONSTRAINT `order_room_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
