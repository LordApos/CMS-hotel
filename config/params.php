<?php

return [
    'admin_email' => 'camelot_hotel@ukr.net',
    'hotel_name' => 'гостиница Camelot',
    'pagination' => 6,
    'smtp_host' => 'smtp.ukr.net',
    'smtp_port' => '2525',
    'smtp_protocol' => 'ssl',
    'smtp_login' => 'camelot_hotel@ukr.net',
    'smtp_password' => 'qwerty12Q',
    'img_width' => 280,
    'img_height' => 250,
    'gallery_width' => 700,
    'gallery_height' => 1000,
    'service_width' => 700,
    'service_height' => 100,
];

